package logcat.net.podcastapp.ui.details

import android.media.MediaPlayer
import android.widget.MediaController

class MediaPlayerControl(val mediaPlayer: MediaPlayer) : MediaController.MediaPlayerControl {
    override fun isPlaying(): Boolean {
        return mediaPlayer.isPlaying
    }

    override fun canSeekForward(): Boolean {
        return true;
    }

    override fun getDuration(): Int {
        return mediaPlayer.duration
    }

    override fun pause() {
        mediaPlayer.pause()
    }

    override fun getBufferPercentage(): Int {
        return 0
    }

    override fun seekTo(pos: Int) {
        mediaPlayer.seekTo(pos)
    }

    override fun getCurrentPosition(): Int {
        return mediaPlayer.currentPosition
    }

    override fun canSeekBackward(): Boolean {
        return true
    }

    override fun start() {
        mediaPlayer.start()
    }

    override fun getAudioSessionId(): Int {
        return mediaPlayer.audioSessionId
    }

    override fun canPause(): Boolean {
        return true
    }

}