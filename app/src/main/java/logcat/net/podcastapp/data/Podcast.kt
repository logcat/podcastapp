package logcat.net.podcastapp.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Podcast(val showId: String, val title: String, val mediaLink: String): Parcelable
