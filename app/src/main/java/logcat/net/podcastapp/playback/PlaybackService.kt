package logcat.net.podcastapp.playback

import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.Binder
import android.os.IBinder

val NOTIFICATION_PODCAST = 1

class PlaybackService : Service() {

    override fun onBind(intent: Intent?): IBinder? {
        return LocalBinder()
    }

    inner class LocalBinder: Binder() {
        fun getPlaybackService(): PlaybackService {
            return this@PlaybackService
        }
    }

    lateinit var mediaPlayer: MediaPlayer

    override fun onCreate() {
        super.onCreate()
        mediaPlayer = MediaPlayer()
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        if (ACTION_STOP.equals(intent.action)) {
            mediaPlayer.reset()
            stopForeground(true)
        }

        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer.release()
    }

    companion object {
        val ACTION_STOP = "stop"
        val CHANNEL_ID = "net.logcat.podcastapp.NOTIFICATION_CHANNEL"
    }

}