package logcat.net.podcastapp.ui.details

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.os.Parcelable
import android.support.v4.app.NotificationCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.widget.MediaController
import kotlinx.android.synthetic.main.activity_podcast_detail.*
import logcat.net.podcastapp.R
import logcat.net.podcastapp.data.Podcast
import logcat.net.podcastapp.playback.NOTIFICATION_PODCAST
import logcat.net.podcastapp.playback.PlaybackService
import logcat.net.podcastapp.ui.PodcastListActivity

/**
 * An activity representing a single Podcast detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a [PodcastListActivity].
 */
class PodcastDetailActivity : AppCompatActivity() {

    private lateinit var podcast: Podcast

    private lateinit var controller: MediaController

    val serviceConnection = object : ServiceConnection {

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            fab.setOnClickListener { view ->
                val playbackService = (service as PlaybackService.LocalBinder).getPlaybackService()
                playbackService.startForeground(NOTIFICATION_PODCAST, showNotification(podcast))
                val mediaPlayer = playbackService.mediaPlayer

                playPodcast(mediaPlayer)
            }
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            fab.setOnClickListener(null)
            controller.hide()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_podcast_detail)
        setSupportActionBar(detail_toolbar)

        podcast = intent.getParcelableExtra<Parcelable>(PodcastDetailFragment.ARG_PODCAST) as Podcast

        controller = MediaController(this);
        controller.setAnchorView(details)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            val fragment = PodcastDetailFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(PodcastDetailFragment.ARG_PODCAST, podcast)
                }
            }

            supportFragmentManager.beginTransaction()
                    .add(R.id.podcast_detail_container, fragment)
                    .commit()
        }
    }

    override fun onResume() {
        super.onResume()
        val playbackServiceIntent = Intent(applicationContext, PlaybackService::class.java)
        startService(playbackServiceIntent)
        bindService(playbackServiceIntent, serviceConnection, Context.BIND_ABOVE_CLIENT)
    }

    override fun onPause() {
        super.onPause()
        unbindService(serviceConnection)
    }

    private fun playPodcast(mediaPlayer: MediaPlayer) {
        try {
            mediaPlayer.reset()

            val mediaUri = Uri.parse(podcast.mediaLink);
            mediaPlayer.setDataSource(applicationContext, mediaUri)
            mediaPlayer.prepareAsync()
            mediaPlayer.setOnPreparedListener {
                mediaPlayer.start()
                controller.setMediaPlayer(MediaPlayerControl(mediaPlayer))
                controller.show(0)
            }

        } catch (exception: Exception) {
            Log.e("PodcastDetailActivity", "Exception while starting playback", exception);
        }
    }


    override fun onOptionsItemSelected(item: MenuItem) =
            when (item.itemId) {
                android.R.id.home -> {
                    // This ID represents the Home or Up button. In the case of this
                    // activity, the Up button is shown. For
                    // more details, see the Navigation pattern on Android Design:
                    //
                    // http://developer.android.com/design/patterns/navigation.html#up-vs-back

                    navigateUpTo(Intent(this, PodcastListActivity::class.java))
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    private fun showNotification(podcast: Podcast): Notification {
        registerNotificationChannel()

        val stopIntent = Intent(applicationContext, PlaybackService::class.java)
        stopIntent.setAction(PlaybackService.ACTION_STOP)
        val pendingStopIntent = PendingIntent.getService(applicationContext, 0, stopIntent, PendingIntent.FLAG_CANCEL_CURRENT)

        val builder = NotificationCompat.Builder(this, PlaybackService.CHANNEL_ID)
                .setSmallIcon(android.R.drawable.ic_media_play)
                .setContentTitle(podcast.title)
                .setSound(null)
                .addAction(android.R.drawable.ic_media_pause, getString(R.string.stop), pendingStopIntent)

        return builder.build()
    }

    private fun registerNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            val name = getString(R.string.notification_channel_name)
            val description = getString(R.string.notification_channel_name)
            val channel = NotificationChannel(PlaybackService.CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT)
            channel.setSound(null, null)
            channel.description = description
            // Register the channel with the system
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }


}
