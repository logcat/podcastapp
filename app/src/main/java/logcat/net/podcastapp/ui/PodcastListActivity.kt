package logcat.net.podcastapp.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import kotlinx.android.synthetic.main.activity_podcast_list.*
import kotlinx.android.synthetic.main.podcast_list_content.view.*
import kotlinx.android.synthetic.main.podcast_list.*
import logcat.net.podcastapp.R
import logcat.net.podcastapp.data.Podcast
import logcat.net.podcastapp.ui.details.PodcastDetailActivity
import logcat.net.podcastapp.ui.details.PodcastDetailFragment

/**
 * An activity representing a list of Pings. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a [PodcastDetailActivity] representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
class PodcastListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_podcast_list)

        setSupportActionBar(toolbar)
        toolbar.title = title

        setupRecyclerView(podcast_list)
    }

    private fun setupRecyclerView(recyclerView: RecyclerView) {
        recyclerView.adapter = SimpleItemRecyclerViewAdapter(this, generatePodcasts())
    }

    class SimpleItemRecyclerViewAdapter(private val parentActivity: PodcastListActivity,
                                        private val values: List<Podcast>) :
            RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder>() {

        private val onClickListener: View.OnClickListener

        init {
            onClickListener = View.OnClickListener { v ->
                val item = v.tag as Podcast
                val intent = Intent(v.context, PodcastDetailActivity::class.java).apply {
                    putExtra(PodcastDetailFragment.ARG_PODCAST, item)
                }
                v.context.startActivity(intent)
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.podcast_list_content, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = values[position]
            holder.idView.text = item.title
            holder.contentView.text = item.title

            with(holder.itemView) {
                tag = item
                setOnClickListener(onClickListener)
            }
        }

        override fun getItemCount() = values.size

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val idView: TextView = view.id_text
            val contentView: TextView = view.content
        }
    }

    fun generatePodcasts(): List<Podcast> {
        val podcasts = ArrayList<Podcast>();
        repeat(3) {
            podcasts.add(Podcast("1", "The Ryan J Radio Show", "http://ryanjradioshow.podomatic.com/enclosure/2012-08-14T05_18_22-07_00.mp3"))
        }
        return podcasts;
    }
}
