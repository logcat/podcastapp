package logcat.net.podcastapp.ui.details

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_podcast_detail.*
import kotlinx.android.synthetic.main.podcast_detail.view.*
import logcat.net.podcastapp.R
import logcat.net.podcastapp.data.Podcast

/**
 * A fragment representing a single Podcast detail screen.
 * This fragment is either contained in a [PodcastListActivity]
 * in two-pane mode (on tablets) or a [PodcastDetailActivity]
 * on handsets.
 */
class PodcastDetailFragment : Fragment() {

    /**
     * The dummy content this fragment is presenting.
     */
    private var podcast: Podcast? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            if (it.containsKey(ARG_PODCAST)) {
                // Load the dummy content specified by the fragment
                // arguments. In a real-world scenario, use a Loader
                // to load content from a content provider.
                podcast = it.getParcelable(ARG_PODCAST)
                activity?.toolbar_layout?.title = podcast?.title
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.podcast_detail, container, false)

        // Show the dummy content as text in a TextView.
        podcast?.let {
            rootView.podcast_detail.text = it.title
        }

        return rootView
    }

    companion object {
        /**
         * The fragment argument representing the podcast ID that this fragment
         * represents.
         */
        const val ARG_PODCAST = "podcast"
    }
}
